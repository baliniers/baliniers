By Charles Le Goffic (1863–1932).
First published in "L'Âme Bretonne", vol. III, 1910.
  https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_bretonne_s%C3%A9rie_3/Le_ch%C3%A2teau_de_Barberine
  https://fr.wikisource.org/wiki/Page:Le_Goffic_-_L%27%C3%82me_bretonne_s%C3%A9rie_3,_1910.djvu/27
Re-published in "Contes de l'Armor et de l'Argoat", 1928.
  http://www.litteratureaudio.com/livre-audio-gratuit-mp3/le-goffic-charles-les-baliniers-de-kerjean.html
  http://www.litteratureaudio.com/forum/textes/le-goffic-charles-les-baliniers-de-kerjean/page-1

Ar c’hoent euz an incardeuret
A zo bet e Kerian savet.
=> « Le premier des baliniers, c’est à Kerjean qu’il fut élevé. »
