﻿# TODO: Translation updated at 2015-06-05 23:28

translate english strings:

    # game/screens.rpy:192
    old "Start Game"
    new "Start Game"

    # game/screens.rpy:193
    old "Load Game"
    new "Load Game"

    # game/screens.rpy:194
    old "Preferences"
    new "Preferences"

    # game/screens.rpy:195
    old "Credits"
    new "Credits"

    # game/screens.rpy:196
    old "Quit"
    new "Quit"

    # game/screens.rpy:224
    old "Return"
    new "Return"

    # game/screens.rpy:226
    old "Save Game"
    new "Save Game"

    # game/screens.rpy:228
    old "Main Menu"
    new "Main Menu"

    # game/screens.rpy:229
    old "Help"
    new "Help"

    # game/screens.rpy:259
    old "Previous"
    new "Previous"

    # game/screens.rpy:262
    old "Auto"
    new "Auto"

    # game/screens.rpy:265
    old "Quick"
    new "Quick"

    # game/screens.rpy:272
    old "Next"
    new "Next"

    # game/screens.rpy:298
    old "Empty Slot."
    new "Empty Slot."

    # game/screens.rpy:357
    old "Display"
    new "Display"

    # game/screens.rpy:358
    old "Window"
    new "Window"

    # game/screens.rpy:359
    old "Fullscreen"
    new "Fullscreen"

    # game/screens.rpy:365
    old "Transitions"
    new "Transitions"

    # game/screens.rpy:366
    old "All"
    new "All"

    # game/screens.rpy:367
    old "None"
    new "None"

    # game/screens.rpy:373
    old "Text Speed"
    new "Text Speed"

    # game/screens.rpy:380
    old "Joystick..."
    new "Joystick..."

    # game/screens.rpy:388
    old "Skip"
    new "Skip"

    # game/screens.rpy:389
    old "Seen Messages"
    new "Seen Messages"

    # game/screens.rpy:390
    old "All Messages"
    new "All Messages"

    # game/screens.rpy:396
    old "Begin Skipping"
    new "Begin Skipping"

    # game/screens.rpy:402
    old "After Choices"
    new "After Choices"

    # game/screens.rpy:403
    old "Stop Skipping"
    new "Stop Skipping"

    # game/screens.rpy:404
    old "Keep Skipping"
    new "Keep Skipping"

    # game/screens.rpy:410
    old "Auto-Forward Time"
    new "Auto-Forward Time"

    # game/screens.rpy:414
    old "Wait for Voice"
    new "Wait for Voice"

    # game/screens.rpy:421
    old "Music Volume"
    new "Music Volume"

    # game/screens.rpy:428
    old "Sound Volume"
    new "Sound Volume"

    # game/screens.rpy:432
    old "Test"
    new "Test"

    # game/screens.rpy:441
    old "Voice Volume"
    new "Voice Volume"

    # game/screens.rpy:444
    old "Voice Sustain"
    new "Voice Sustain"

    # game/screens.rpy:500
    old "Yes"
    new "Yes"

    # game/screens.rpy:501
    old "No"
    new "No"

    # game/screens.rpy:526
    old "Back"
    new "Back"

    # game/screens.rpy:527
    old "Save"
    new "Save"

    # game/screens.rpy:528
    old "Q.Save"
    new "Q.Save"

    # game/screens.rpy:529
    old "Q.Load"
    new "Q.Load"

    # game/screens.rpy:531
    old "F.Skip"
    new "F.Skip"

    # game/screens.rpy:533
    old "Prefs"
    new "Prefs"

