﻿# Translation: ?
# Proofreading: ?

# game/script.rpy:13
translate english start_a19225e1:

    # e "Vous venez de créer un nouveau jeu Ren'Py."
    e "You've created a new Ren'Py game."

# game/script.rpy:15
translate english start_ae16e0d6:

    # e "Après avoir ajouté une histoire, des images et de la musique, vous pouvez le présenter au monde entier!"
    e "Once you add a story, pictures, and music, you can release it to the world!"

