﻿### Les Baliniers de Kerjean

##
# Personnages
##

# René Barbier de Kerjean: fier, viril, noble, sérieux
# - expressions: confiant, sérieux, livide, furieux
define r = Character(_('René'), color="#c8ffc8", image="rene", window_left_padding=350)
image side rene = "rene-side.png"

# Françoise de Quélen: jeune, aimante, noble, avec du maintien, réfléchie
# - expressions: soucieuse, ravie, pincée, ironique
define f = Character(_('Françoise'), color="#c8ffc8", image="françoise")
image françoise base = "francoise-base.png"
image françoise base2 = "francoise-base2.png"  # test meilleur zoom

# La Reine: imbue d'elle-même, puissante, cynique, un peu d'aigreur, un peu blasée
# - sur le trône (organiser l'inscrustation dans le décor)
# - sur le trône, avec pigeon voyageur sur le doigt

# Bombelles, Saint-Phar, Bruc, Belz: séducteurs, riches/aisés, un certaine vulgarité
# - en groupe, se moquant
# - en groupe, surpris travaillant (filant / opérant la tisserie)


##
# Décors
##
# porte chateau, matin (départ)
image bg chateau interieur porte = "chateau-interieur-porte.png"
# porte château, soir (retour)
# salle du trône
# CG/animation: départ train, château en fond
# CG: retour au château
# CG: tisserie
# CG: image à mettre avec le générique


##
# Scenario
##

# Ton: fable morale, humour bon enfant sur la fin

# Scene (porte, matin): exposition
# - explication des raisons du départ
# - promesse de retrouvailles
# Transition: corbeaux sur l'écusson des Barbier, animation train de profil, écran titre
# Scene (trône): réception par la reine
# - les muguets en arrière plan, se manquant
# - introduction des muguets
# - pari
# Scene (trône): 1 semaine / sachet:
# - René se comporte de manière confiante à la cour
# - sachet 1: vidame de Bombelles, ruban de soie bleue (ruban à cheveux)
# - René se sent prisonnier, mais réaffirme sa confiance à Françoise intérieurement
# - sachet 2: chevalier de Saint-Phar, épingle d'or (épingle à guimpe/vêtement)
# - sachet 3: comte de Bruc, boucle de cheveux blonds
# - sachet 4: marquis de Belz, bague/alliance
# Transition: retour précipité, CG château
# Scene (porte, soir): Françoise raconte
# Scene (tisserie): René découvre les muguets
# Fin: René élevé au rang de marquis (CG: lettre, cérémonie/adoubement, couronne marquisale?) pendant que les crédits défilent

# Adaptation SteamPunk:
# - chevaux -> train
# - messager -> pigeon voyageur, avec bonnet aviateur + lunettes
# - retour à cheval -> mini-dirigeable? ou le train à nouveau?

label start:

    scene bg chateau interieur porte

    return
    
label test:
    
    scene bg chateau interieur porte

    "Au château de Kerjean..."

    show françoise base
    f "René, mon cher seigneur,"
    f base2 "revenez-moi vite..."
    show françoise
    r "Je rentrerai bien vite, mon coeur, et nous ne nous quitterons plus."

    "Texte très long pour tester la boîte de dialogue 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 "

    return
